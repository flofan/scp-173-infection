﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smod2;
using Smod2.Attributes;
using Smod2.EventHandlers;

namespace SCP_173_Infection
{
    [PluginDetails( 
        author = "Flo - Fan",
        description = "A Minigame",
        id = "flo.173.infection",
        name = "SCP 173 Infection",
        SmodMajor = 3,
        SmodMinor = 1,
        SmodRevision = 19,
        version = "1.0.0"
        )]

    public class Peanut : Plugin
    {
        public override void OnDisable()
        {
            this.Info("The plugin is now @#fg=Red;Disabled.");
        }

        public override void OnEnable()
        {
            this.Info("The plugin is now @#fg=Green;Enabled.");
        }

        public override void Register()
        {
            this.AddConfig(new Smod2.Config.ConfigSetting("pean_enable", true, Smod2.Config.SettingType.BOOL, true, "Enable the plugin"));
            if (ConfigManager.Manager.Config.GetBoolValue("pean_enable", true))
            {
                this.AddEventHandlers(new PluginEvent(this));

                this.AddConfig(new Smod2.Config.ConfigSetting("pean_173_hp", 2000, Smod2.Config.SettingType.NUMERIC, true, "Set 173 HP"));
                this.AddConfig(new Smod2.Config.ConfigSetting("pean_173_tesla", false, Smod2.Config.SettingType.BOOL, true, "Active tesla for SCP 173"));


                this.AddConfig(new Smod2.Config.ConfigSetting("pean_classd_hp", 120, Smod2.Config.SettingType.NUMERIC, true, "Set Class D HP"));
                this.AddConfig(new Smod2.Config.ConfigSetting("pean_classd_ammo", 120, Smod2.Config.SettingType.NUMERIC, true, "Set Class D ammo"));
                this.AddConfig(new Smod2.Config.ConfigSetting("pean_classd_tesla", false, Smod2.Config.SettingType.BOOL, true, "Active tesla for Class D Personnel"));

                this.AddConfig(new Smod2.Config.ConfigSetting("pean_ic_hp", 250, Smod2.Config.SettingType.NUMERIC, true, "Set IC HP"));
                this.AddConfig(new Smod2.Config.ConfigSetting("pean_ic_ammo", 200, Smod2.Config.SettingType.NUMERIC, true, "Set IC ammo"));
                this.AddConfig(new Smod2.Config.ConfigSetting("pean_ic_tesla", false, Smod2.Config.SettingType.BOOL, true, "Active tesla for IC Personnel"));

            }
        }
    }
}
